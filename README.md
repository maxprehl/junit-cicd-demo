# JUnit CICD Demo

## What you will need for this project:

- A version of Java Installed on your Computer (Tutorial uses Java 11)
- JUnit-platform-console-standalone downloaded and placed inside the project folder
  - https://search.maven.org/search?q=g:org.junit.platform%20AND%20a:junit-platform-consolestandalone%20AND%20v:1.8.0-M1
- The Ability to create and execute makefiles
- A GitLab account
- A code editor
