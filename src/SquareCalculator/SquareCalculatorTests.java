
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SquareCalculatorTests {
  @Test
  @DisplayName("1 * 1 = 1")
  void squaresOne() {
    SquareCalculator squareCalculator = new SquareCalculator();
    assertEquals(1, squareCalculator.squareNumber(1), "1 * 1 should equal 1");
  }

  @Test
  @DisplayName("2 * 2 = 4")
  void squaresTwo() {
    SquareCalculator squareCalculator = new SquareCalculator();
    assertEquals(4, squareCalculator.squareNumber(2), "2 * 2 should equal 4");
  }

  @Test
  @DisplayName("4 * 4 = 16")
  void squaresFour() {
    SquareCalculator squareCalculator = new SquareCalculator();
    assertEquals(16, squareCalculator.squareNumber(4), "4 * 4 should equal 16");
  }
}
