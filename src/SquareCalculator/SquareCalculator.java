
public class SquareCalculator {

  public int squareNumber(int num) {
    return num * num;
  }
  public static void main(String[] args) throws Exception {
    SquareCalculator squareCalculator = new SquareCalculator();
    if(args.length != 1) {
      System.out.println("Invalid number of arguments. Input required is a single integer value.");
      throw new Exception("Invalid Input");
    }
    for(int i = 0; i < args.length; i++) {
      int numberInput = Integer.parseInt(args[0]);
      System.out.println(squareCalculator.squareNumber(numberInput));
    }
  }
}